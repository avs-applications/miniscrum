// @ts-check

import {
  LOAD_TASKS,
  NEW_TASK,
  EDIT_TASK,
  CANCEL_EDIT_TASK,
  ADD_TASK,
  UPDATE_TASK,
  ARCHIVE_TASK,
  DELETE_TASK,
  UPDATE_TASK_SEARCH,
  ERROR_LOAD_TASKS,
  ERROR_ADD_TASKS,
  ERROR_UPDATE_TASKS,
  ERROR_DELETE_TASKS,
  HIDE_ERROR
} from "../actions/actionTypes";

import { ARCHIVE } from "../../../const";

const initialState = {
  all: [],
  search: "",
  showDialog: false,
  showDialogTask: null,
  errors: [],
  error_load: null
};

const tasks = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_TASKS: {
      return {
        ...state,
        error_load: null,
        all: action.payload
      };
    }
    case NEW_TASK: {
      return {
        ...state,
        showDialog: true,
        showDialogTask: null
      };
    }
    case EDIT_TASK: {
      return {
        ...state,
        showDialog: true,
        showDialogTask: action.payload
      };
    }
    case CANCEL_EDIT_TASK: {
      return {
        ...state,
        showDialog: false
      };
    }
    case ADD_TASK: {
      return {
        ...state,
        showDialog: false,
        showDialogTask: null,
        all: [...state.all, action.payload]
      };
    }
    case UPDATE_TASK: {
      const updatedTask = action.payload;
      return {
        ...state,
        showDialog: false,
        showDialogTask: null,
        all: [
          ...state.all.filter(task => task._id !== updatedTask._id),
          updatedTask
        ]
      };
    }
    case ARCHIVE_TASK: {
      const id = action.payload;
      const originTask = state.all.find(task => task._id === id);
      originTask.sprint = ARCHIVE;
      return {
        ...state,
        all: [...state.all.filter(task => task._id !== id), originTask]
      };
    }
    case DELETE_TASK: {
      return {
        ...state,
        all: state.all.filter(task => task._id !== action.payload),
        showDialog: false,
        showDialogTask: null
      };
    }
    case UPDATE_TASK_SEARCH: {
      return {
        ...state,
        search: action.payload
      };
    }
    case ERROR_LOAD_TASKS: {
      return {
        ...state,
        error_load: action.payload
      };
    }
    case ERROR_ADD_TASKS: {
      const error = {
        index: state.errors.length,
        visible: true,
        message: action.payload
      };
      return {
        ...state,
        errors: [...state.errors, error]
      };
    }
    case ERROR_UPDATE_TASKS: {
      const error = {
        index: state.errors.length,
        visible: true,
        message: action.payload
      };
      return {
        ...state,
        errors: [...state.errors, error]
      };
    }
    case ERROR_DELETE_TASKS: {
      const error = {
        index: state.errors.length,
        visible: true,
        message: action.payload
      };
      return {
        ...state,
        errors: [...state.errors, error]
      };
    }
    case HIDE_ERROR: {
      console.log(state.errors);
      const index = action.payload;
      const error = state.errors[index];
      error.visible = false;
      return {
        ...state,
        errors: [...state.errors.filter(error => error.index !== index), error]
      };
    }
    default:
      return state;
  }
};

export default tasks;
