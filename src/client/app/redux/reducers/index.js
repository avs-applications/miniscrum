// @ts-check

import { combineReducers } from "redux";

import tasks from "./tasks";
import sprints from "./sprints";

export default combineReducers({ tasks, sprints });
