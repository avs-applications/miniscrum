// @ts-check

import { CANCEL_EDIT_TASK } from "../actionTypes";

const cancelEditTask = () => ({
  type: CANCEL_EDIT_TASK
});

export default cancelEditTask;
