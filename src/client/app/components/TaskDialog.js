// @ts-check

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { withStyles } from "@material-ui/core/styles";

import addTask from "../redux/actions/tasks/addTask";
import updateTask from "../redux/actions/tasks/updateTask";
import deleteTask from "../redux/actions/tasks/deleteTask";
import cancelEditTask from "../redux/actions/tasks/cancelEditTask";

import SingleSelectInput from "./SingleSelectInput";
import MultiSelectInput from "./MultiSelectInput";

import taskValidator from "../../../shared/validation/taskValidator";

const styles = theme => ({
  appBar: {
    position: "relative"
  },
  flex: {
    flex: 1
  },
  grow: {
    flexGrow: 1
  },
  container: {
    margin: 15
  },
  textField: {
    marginTop: 15
  }
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class TaskDialog extends Component {
  state = {
    name: "",
    sprint: {
      label: "",
      value: ""
    },
    story: {
      label: "",
      value: ""
    },
    description: "",
    tags: [],
    errors: new Map()
  };

  updateState = () => {
    const task = this.props.task;
    if (task) {
      this.setState({
        name: task.name,
        sprint: {
          value: task.sprint,
          label: task.sprint
        },
        story: {
          value: task.story,
          label: task.story
        },
        description: task.description,
        tags: [],
        errors: new Map()
      });
    } else {
      this.setState({
        name: "",
        sprint: {
          label: "",
          value: ""
        },
        story: {
          label: "",
          value: ""
        },
        description: "",
        tags: [],
        errors: new Map()
      });
    }
  };

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  handleChangeStory = story => {
    this.setState({
      story: story
    });
  };

  handleChangeSprint = sprint => {
    this.setState({
      sprint: sprint
    });
  };

  handleChangeTags = tags => {
    this.setState({
      tags: tags
    });
  };

  handleReset = event => {
    this.updateState();
  };

  handleDelete = event => {
    const task = this.props.task;
    if (task) {
      this.props.deleteTask(task._ids);
    }
  };

  handleNewTask = event => {
    this.props.newTask();
  };

  handleSubmit = event => {
    const { name, sprint, story, description } = this.state;
    const task = this.props.task;
    const validation = taskValidator({
      name,
      sprint: sprint.value,
      story: story.value
    });
    if (validation.size > 0) {
      this.setState({ ...this.state, errors: validation });
    } else {
      if (task) {
        const id = task._id;
        this.props.updateTask(id, name, sprint.value, story.value, description);
      } else {
        this.props.addTask(name, sprint.value, story.value, description);
      }
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.task !== prevProps.task) {
      this.updateState();
    }
  }

  handleClose = () => {
    this.props.cancelEditTask();
  };

  render() {
    const { classes } = this.props;
    const task = this.props.task;
    const tasks = this.props.tasks;

    const sprints = Array.from(
      new Set(
        tasks
          .map(task => task.sprint)
          .filter(sprint => sprint !== "")
          .sort()
      )
    ).map(sprint => {
      return {
        value: sprint,
        label: sprint
      };
    });

    const stories = Array.from(
      new Set(
        tasks
          .map(task => task.story)
          .filter(story => story !== "")
          .sort()
      )
    ).map(story => {
      return {
        value: story,
        label: story
      };
    });

    const tags = [
      {
        value: "tag1",
        label: "tag1"
      },
      {
        value: "tag2",
        label: "tag2"
      }
    ];

    return (
      <Dialog
        fullScreen
        open={this.props.show}
        onClose={this.handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              onClick={this.handleClose}
              aria-label="Close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="headline" color="inherit" noWrap>
              {task ? "Edit task" + task.name : "Create new task"}
            </Typography>
            <div className={classes.grow} />
            <Button color="inherit" onClick={this.handleSubmit}>
              save
            </Button>
            <Button color="secondary" onClick={this.handleReset}>
              reset
            </Button>
            <Button
              disabled={!!this.props.task}
              color="secondary"
              onClick={this.handleDelete}
            >
              delete
            </Button>
          </Toolbar>
        </AppBar>

        <form className={classes.container} noValidate autoComplete="off">
          <TextField
            name="name"
            label="Name"
            error={this.state.errors.has("name")}
            helperText={this.state.errors.get("name")}
            className={classes.textField}
            fullWidth
            value={this.state.name}
            onChange={this.handleChange}
          />
          <SingleSelectInput
            name="sprint"
            label="Sprint"
            error={this.state.errors.has("sprint")}
            errorText={this.state.errors.get("sprint")}
            options={sprints}
            onChange={this.handleChangeSprint}
            value={this.state.sprint}
            placeholder="Sprint"
          />
          <SingleSelectInput
            name="story"
            label="Story"
            error={this.state.errors.has("story")}
            errorText={this.state.errors.get("story")}
            options={stories}
            onChange={this.handleChangeStory}
            value={this.state.story}
            placeholder="Story"
          />
          <TextField
            name="description"
            label="Description"
            multiline
            rows="5"
            className={classes.textField}
            fullWidth
            value={this.state.description}
            onChange={this.handleChange}
          />
          <MultiSelectInput
            name="tags"
            label="Tags"
            options={tags}
            onChange={this.handleChangeTags}
            value={this.state.tags}
            placeholder="Tags"
          />
        </form>
      </Dialog>
    );
  }
}

TaskDialog.propTypes = {
  show: PropTypes.bool.isRequired,
  task: PropTypes.object.isRequired,
  tasks: PropTypes.array,
  addTask: PropTypes.func,
  updateTask: PropTypes.func,
  deleteTask: PropTypes.func,
  cancelEditTask: PropTypes.func
};

const mapStateToProps = state => {
  return {
    show: state.tasks.showDialog,
    task: state.tasks.showDialogTask,
    tasks: state.tasks.all
  };
};

const mapDispatchToProps = {
  addTask,
  updateTask,
  deleteTask,
  cancelEditTask
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
  // @ts-ignore
)(withStyles(styles)(TaskDialog));
