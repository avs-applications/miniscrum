// @ts-check

import React from "react";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";

import Header from "./components/Header";
import Errors from "./components/Errors";
import Backlog from "./components/Backlog";
import Sprint from "./components/Sprint";
import Footer from "./components/Footer";
import TaskDialog from "./components/TaskDialog";
import { BACKLOG } from "../const";

const App = ({ tasks }) => {
  const backlogTasks = tasks.filter(task => task.sprint === BACKLOG);
  const sprintTasks = tasks.filter(task => task.sprint !== BACKLOG);
  return (
    <React.Fragment>
      <Header />
      <Errors />
      <div id="content">
        <Grid container spacing={8} justify="center">
          <Grid item xs={4}>
            <Backlog tasks={backlogTasks} />
          </Grid>
          <Grid item xs={8}>
            <Sprint tasks={sprintTasks} />
          </Grid>
        </Grid>
        <TaskDialog />
      </div>
      <Footer />
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    tasks: state.tasks.all
  };
};

export default connect(mapStateToProps)(App);
