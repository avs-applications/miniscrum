// @ts-check

const logger = require("../logger");
const database = require("../database");
const taskValidator = require("../../shared/validation/taskValidator");

const controller = function addTaskController() {
  function controller(req, res) {
    let name = req.body.name;
    let sprint = req.body.sprint;
    let story = req.body.story;
    let description = req.body.description;
    const validation = taskValidator({ name, sprint, story });
    if (validation.size > 0) {
      res.status(404).send(validation);
    } else {
      database
        .insertTask(name, sprint, story, description)
        .then(task => {
          res.send(task);
        })
        .catch(reason => {
          const error = {
            message: `Error insert task (name: ${name}, sprint: ${sprint}, story: ${story}, description: ${description})`
          };
          logger.error(`${error.message}. Error: ${reason}`);
          res.status(500).send(error);
        });
    }
  }
  return controller;
};

module.exports = controller;
