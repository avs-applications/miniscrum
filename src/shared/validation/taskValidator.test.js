// @ts-check

describe("Test task validator", () => {
  const taskValidator = require("./taskValidator");

  const testTask = {
    name: "Test Task",
    sprint: "Test Sprint",
    story: "Test Story"
  };

  test("Name must be a string", () => {
    expect(taskValidator({ ...testTask, name: undefined })).toEqual(
      new Map([["name", "name must be a string"]])
    );
  });

  test("Name can't be empty string", () => {
    expect(taskValidator({ ...testTask, name: "" })).toEqual(
      new Map([["name", "name can't be empty"]])
    );
  });

  test("Sprint must be a string", () => {
    expect(taskValidator({ ...testTask, sprint: undefined })).toEqual(
      new Map([["sprint", "sprint must be a string"]])
    );
  });

  test("Sprint can't be empty string", () => {
    expect(taskValidator({ ...testTask, sprint: "" })).toEqual(
      new Map([["sprint", "sprint can't be empty"]])
    );
  });

  test("Story must be a string", () => {
    expect(taskValidator({ ...testTask, story: undefined })).toEqual(
      new Map([["story", "story must be a string"]])
    );
  });

  test("Story can't be empty string", () => {
    expect(taskValidator({ ...testTask, story: "" })).toEqual(
      new Map([["story", "story can't be empty"]])
    );
  });

  test("Name, Sprint and Story can't be empty", () => {
    expect(taskValidator({ name: "", sprint: "", story: "" })).toEqual(
      new Map([
        ["name", "name can't be empty"],
        ["sprint", "sprint can't be empty"],
        ["story", "story can't be empty"]
      ])
    );
  });
});
