// @ts-check

/**
 * Task validator error format
 * @typedef {Map} errorDescription
 * @property {string} key
 * @property {string} value
 */

/**
 * Validate task fields
 * @param {Object} task
 * @param {string} task.name
 * @param {string} task.sprint
 * @param {string} task.story
 * @returns {errorDescription} - Array with field names and error description
 */
const taskValidator = task => {
  const errors = new Map();
  if (typeof task.name !== "string") {
    errors.set("name", "name must be a string");
  } else if (task.name.trim() === "") {
    errors.set("name", "name can't be empty");
  }

  if (typeof task.sprint !== "string") {
    errors.set("sprint", "sprint must be a string");
  } else if (task.sprint.trim() === "") {
    errors.set("sprint", "sprint can't be empty");
  }

  if (typeof task.story !== "string") {
    errors.set("story", "story must be a string");
  } else if (task.story.trim() === "") {
    errors.set("story", "story can't be empty");
  }
  return errors;
};

module.exports = taskValidator;
